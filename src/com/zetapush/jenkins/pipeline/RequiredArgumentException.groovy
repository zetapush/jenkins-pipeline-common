package com.zetapush.jenkins.pipeline

class RequiredArgumentException extends IllegalArgumentException {
	RequiredArgumentException(String argName, String addtionnalInfo = "") {
		super("${argName} is required. ${addtionnalInfo}")
	}
}
