package com.zetapush.jenkins.pipeline.file

import groovy.json.StringEscapeUtils

class ShFileHandler implements FileHandler {
    private String encoding
    private Object context
    private Object env
    private boolean debugMode

    ShFileHandler(encoding, context, env, debugMode = false) {
        this.encoding = encoding
        this.context = context
        this.env = env
        this.debugMode = debugMode
    }

    @Override
    void create(File file) {
        context.sh "mkdir -p ${file.parent}"
        context.sh "sh -c \"echo -e '' > ${file}\""
    }

    @Override
    void empty(File file) {
        create(file)
    }

    @Override
    void append(File file, String content, String encoding, boolean newLine) {
        context.sh "sh -c \"echo -e '${encode(content)}' >> ${file}\""
    }

    @Override
    void append(File file, List lines, String encoding) {
        lines.forEach({ l -> this.append(file, l, encoding, true) })
    }

    @Override
    boolean exists(File file) {
        String res = context.sh script: "test -f ${file} && echo 'true' || echo 'false'", returnStdout: true
        if(debugMode) {
            context.echo "is ${file} exists ? ${res}"
        }
        return Boolean.valueOf(res)
    }

    static String encode(String s) {
        // s comes from Jenkinsfile => 'utf-8'
        byte[] utf8 = s.getBytes("UTF-8")
        String encoded = ''
        for(byte b : utf8) {
            encoded += '\\x'+byteToHex(b)
        }
        return encoded
        //return native2ascii(s)
    }

    /**
     * Encode a String like äöü to \u00e4\u00f6\u00fc
     *
     * @param text
     * @return
     */
    static String native2ascii(String text) {
        if (text == null)
            return text;
        StringBuilder sb = new StringBuilder();
        for (char ch : text.toCharArray()) {
            sb.append(native2ascii(ch));
        }
        return sb.toString();
    }

    /**
     * Encode a Character like ä to \u00e4
     *
     * @param ch
     * @return
     */
    static String native2ascii(char ch) {
        StringBuilder sb = new StringBuilder();
        // write \udddd
        sb.append("\\0");
        StringBuffer hex = new StringBuffer(Integer.toHexString((int) ch));
        hex.reverse();
        int length = 4 - hex.length();
        for (int j = 0; j < length; j++) {
            hex.append('0');
        }
        for (int j = 0; j < 4; j++) {
            sb.append(hex.charAt(3 - j));
        }
        return sb.toString();
    }

    static String byteToHex(byte b) {
        // Returns hex String representation of byte b
        char[] hexDigit = [
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
        ]
        char[] array = [ hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] ]
        return new String(array);
    }
}