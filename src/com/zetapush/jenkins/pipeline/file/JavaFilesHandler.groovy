package com.zetapush.jenkins.pipeline.file

import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption

class JavaFilesHandler implements FileHandler {
    @Override
    void create(File file) {
        Files.createDirectories(Paths.get(file).parent)
        Files.createFile(Paths.get(file))
    }

    @Override
    void empty(File file) {
        create(Paths.get(file))
    }

    @Override
    void append(File file, String content, String encoding, boolean newLine) {
        append(Paths.get(file), [content], encoding, newLine)
    }

    @Override
    void append(File file, List lines, String encoding) {
        Files.write(Paths.get(file), lines, StandardOpenOption.APPEND)
    }

    @Override
    boolean exists(File file) {
        return Files.exists(Paths.get(file))
    }
}