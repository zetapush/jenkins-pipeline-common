package com.zetapush.jenkins.pipeline.file

interface FileHandler extends Serializable {
    void create(File file)

    void empty(File file)

    void append(File file, String content, String encoding, boolean newLine)

    void append(File file, List lines, String encoding)

    boolean exists(File file)
}