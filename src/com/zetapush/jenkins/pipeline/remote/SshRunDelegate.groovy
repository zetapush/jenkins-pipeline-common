package com.zetapush.jenkins.pipeline.remote

import com.zetapush.jenkins.pipeline.StandardMethodsDelegate
import com.zetapush.jenkins.pipeline.remote.SshRunHelper

public class SshRunDelegate extends StandardMethodsDelegate {
	private Map params
	private String dir
	private Object env
	private Object context
	private SshRunHelper helper
	
	SshRunDelegate(Map params, String dir, Object env, Object context, SshRunHelper helper) {
		super(context)
		this.params = params
		this.dir = dir
		this.env = env
		this.context = context
		this.helper = helper
	}

	def sh(String cmd) {
		return context.sh(helper.sshCommand(dir ? "cd '${dir}'; ${cmd}" : cmd))
	}

	def sh(Map args) {
		Map newArgs = [:]
		String cmd = args.script
		newArgs.putAll(args)
		newArgs.script = helper.sshCommand(dir ? "cd '${dir}'; ${cmd}" : cmd)
		return context.sh(newArgs)
	}

	void workdir(String dir, Closure cl) {
		cl.delegate = new SshRunDelegate(params, dir, env, context, helper)
		cl.resolveStrategy = Closure.DELEGATE_ONLY
		cl()
	}
	
	def withCredentials(params, Closure body) {
		body.delegate = this
		body.resolveStrategy = Closure.DELEGATE_ONLY
		return context.withCredentials(params, body)
	}
}
