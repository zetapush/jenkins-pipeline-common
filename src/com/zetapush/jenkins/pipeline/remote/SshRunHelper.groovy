package com.zetapush.jenkins.pipeline.remote

public class SshRunHelper {
	private String server
	private Object port
	private String sshUsername
	private Map extraEnv

	SshRunHelper(String server, Object port, String sshUsername, Map extraEnv) {
		this.server = server
		this.port = port
		this.sshUsername = sshUsername
		this.extraEnv = extraEnv
	}

	String sshCommand(server, port, command) {
		return "ssh -o StrictHostKeyChecking=no  ${sshUsername ? sshUsername+'@' : ''}${server} -p ${port} \"${extraEnvCmd()}${escapeCmd(command)}\""
	}

	String sshCommand(command) {
		return sshCommand(server, port, command)
	}

	String scpCommand(server, port, files, remoteDirectoryCopy) {
		return "scp -o StrictHostKeyChecking=no  -P ${port} -r ${files} ${sshUsername ? sshUsername+'@' : ''}${server}:${remoteDirectoryCopy}"
	}

	String scpCommand(files, remoteDirectoryCopy) {
		return scpCommand(server, port, files, remoteDirectoryCopy)
	}

	def escapeCmd(str) {
		return str.replaceAll('(["])', '\\\\$1')
	}
	
	def extraEnvCmd() {
		if(extraEnv.isEmpty()) {
			return ''
		}
		def str = ''
		for(def entry : extraEnv.entrySet()) {
			str += "export ${entry.key}=${entry.value}; "
		}
		return str
	}
}