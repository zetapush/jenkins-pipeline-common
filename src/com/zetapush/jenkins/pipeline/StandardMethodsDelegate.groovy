package com.zetapush.jenkins.pipeline

import java.util.Map

class StandardMethodsDelegate {
	Object standardCtx
	
	StandardMethodsDelegate(standardCtx) {
		this.standardCtx = standardCtx
	}
	
	void echo(String message) {
		standardCtx.echo message
	}

	def usernamePassword(params) {
		return standardCtx.usernamePassword(params)
	}

	def certificate(params) {
		return standardCtx.certificate(params)
	}
	
	def azureServicePrincipal(params) {
		return standardCtx.azureServicePrincipal(params)
	}
	
	def dockerCert(params) {
		return standardCtx.dockerCert(params)
	}
	
	def file(params) {
		return standardCtx.file(params)
	}
	
	def sshUserPrivateKey(params) {
		return standardCtx.sshUserPrivateKey(params)
	}
	
	def string(params) {
		return standardCtx.string(params)
	}
	
	def usernameColonPassword(params) {
		return standardCtx.usernameColonPassword(params)
	}
	
	def zip(params) {
		return standardCtx.zip(params)
	}
	
	void kill(Map args) {
		standardCtx.kill(args)
	}
}