package com.zetapush.jenkins.pipeline.matcher;

import java.util.function.Predicate;

public class FixedMatcher implements MatcherPredicate<String> {
	private boolean matches;
	
	public FixedMatcher(boolean matches) {
		this.matches = matches;
	}

	@Override
	public boolean test(String line) {
		return matches;
	}
}

