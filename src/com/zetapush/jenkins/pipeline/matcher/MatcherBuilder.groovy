package com.zetapush.jenkins.pipeline.matcher;

import java.util.List;


public class MatcherBuilder implements Serializable {
	private List<String> terms;
	
	public MatcherBuilder(List<String> terms) {
		this.terms = terms;
	}
	
	MatcherPredicate<String> build() {
		And matcher = new And();
		for(String term : terms) {
			matcher.and(new TermMatcher(term));
		}
		return matcher;
	}
}