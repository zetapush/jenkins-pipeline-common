package com.zetapush.jenkins.pipeline.matcher;

import java.io.Serializable;

interface MatcherPredicate<T> extends Serializable {
	boolean test(T obj);
}