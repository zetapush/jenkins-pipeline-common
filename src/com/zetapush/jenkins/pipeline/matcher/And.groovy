package com.zetapush.jenkins.pipeline.matcher;

class And<T> implements MatcherPredicate<T> {
	private List delegates
	
	And(List<MatcherPredicate<T>> delegates = []) {
		this.delegates = delegates
	}
	
	@Override
	public boolean test(T obj) {
		for(T delegate : delegates) {
			if(!delegate.test(obj)) {
				return false
			}
		}
		return true
	}
	
	And<T> and(MatcherPredicate<T> delegate) {
		delegates.add(delegate)
		return this
	}
}