package com.zetapush.jenkins.pipeline.matcher;

import java.io.Serializable
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TermMatcher implements MatcherPredicate<String> {
	private Pattern term;
	
	public TermMatcher(String term) {
		this.term = Pattern.compile(term.replaceAll("([*?])", '.$1'));
	}

	@Override
	public boolean test(String line) {
		Matcher m = term.matcher(line);
		return m.find();
	}
}