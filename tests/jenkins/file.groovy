def execute(env = [:]) {
    this.env = env

    node() {
        withFile(path: 'com.zetapush.ci/zms.properties') {
            append '# specific username/password for Jenkins account'
            append "zms.username=zmsUsername"
            append "zms.password=zmsPassword"
        }
    }
}