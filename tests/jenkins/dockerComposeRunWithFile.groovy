def execute(env = [:]) {
    this.env = env

    node() {

      stage('a') {
          dockerComposeRun(service: 'zms', args: '-u 0:0') {
              withFile(path: 'com.zetapush.ci/in-docker') {
                  append '# specific username/password for Jenkins account'
                  append "zms.username=zmsUsername"
                  append "zms.password=zmsPassword"
              }

              workdir("relative/path") {
                  withFile(path: 'com.zetapush.ci/in-docker-workdir') {
                      append '# specific username2/password2 for Jenkins account'
                      append "zms.username2=zmsUsername2"
                      append "zms.password2=zmsPassword2"
                  }
              }
          }
          withFile(path: 'com.zetapush.ci/out-of-docker') {
              append '# specific username3/password3 for Jenkins account'
              append "zms.username3=zmsUsername3"
              append "zms.password3=zmsPassword3"
          }
      }
    }
}