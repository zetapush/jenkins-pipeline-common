def execute(env = [:]) {
    this.env = env

    node() {
		deploySsh(
			files: "server-parent/zbo/target/zbo.war",
			credentials: 'zetapush-dev-server-zbo',
			server: "toto@zbo-server",
			remoteDirectory: "zbo-dir/2.8.0",
			keep: [num: 3, matching: '*']
		) {}
    }
}