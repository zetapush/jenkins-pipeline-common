def execute() {
    node() {
        env = [
            "ZP_DEMO_SERVER_URL": "http://demo-1.zpush.io",
            "JENKINS_ZP_DEMO_ACCOUNT_CREDENTIALS_ID": "zp-demo"
        ]
      stage('create sandbox') {
        createSandbox(alias: 'alias', server: 'demo', name: 'test', description: '') {
          sandboxLimits(
            limits: [[
              ES_MAX_DOC_SIZE: '+1000',
              GDA_MAX_CELL_SIZE: 100000,
              RAM_PER_REQUEST: '*100'
            ]]
          )
        }
      }
    }
}