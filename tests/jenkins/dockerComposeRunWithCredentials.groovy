def execute(env = [:]) {
    this.env = env

    node() {

      stage('a') {
          dockerComposeRun(service: 'zms', args: '-u 0:0') {
              sh "echo 'dockerComposeRun:zms:sh-1'"
              debugEnv(name: 'dockerComposeRun:zms:debug')

              withCredentials([usernamePassword(credentialsId: 'jenkins-zp-ux-account', usernameVariable: 'zmsUsername', passwordVariable: 'zmsPassword')]) {
                  sh "echo 'dockerComposeRun:withCredentials:sh-1'"
                  sh "echo 'dockerComposeRun:withCredentials:sh-2'"
                  debugEnv(name: 'dockerComposeRun:zms:withCredentials:debug')
              }

              sh "echo 'dockerComposeRun:zms:sh-2'"
          }
      }
    }
}