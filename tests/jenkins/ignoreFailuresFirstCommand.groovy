def execute(env = [:]) {
    this.env = env

    node() {
        ignoreFailures(failFirstError: true) {
            sh 'cmd1 succeeds'
            sh 'cmd2 failing'
            sh 'cmd3 succeeds'
            sh 'cmd4 failing'
            sh 'cmd5 succeeds'
        }
    }
}