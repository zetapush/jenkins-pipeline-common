def execute(env = [:]) {
	this.env = env

	node() {
		def cl = kill.asClosure()
		cl.delegate = delegate
		cl(terms: ['java', 'Thumbnailer'], all: true)
	}
}