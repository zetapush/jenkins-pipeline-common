def execute(env = [:]) {
    this.env = env

    node() {
		signAndroidApks(
            apksToSign: 'platforms/android/build/outputs/apk/android-release-unsigned.apk',
            keyStoreId: 'signapk-certificate-dev',
			overrideContext: this
        )
    }
}