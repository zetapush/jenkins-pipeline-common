def execute(env = [:]) {
    this.env = env

    node() {
		dockerComposeRun(service: 'zms', args: '-u 0:0') {
	        signAndroidApks(
	            apksToSign: 'platforms/android/build/outputs/apk/android-release-unsigned.apk',
	            keyStoreId: 'signapk-certificate-dev'
	        )
		}
    }
}