def execute(env = [:]) {
    this.env = env

    node() {
		deploySsh(
			files: "server-parent/zbo/target/zbo.war",
			credentials: 'zetapush-dev-server-zbo',
			server: "toto@zbo-server",
			remoteDirectory: "zbo-dir/2.8.0"
		) {
			workdir("/var/lib/jetty") {
				sh 'bin/jetty.sh stop'
				sh 'rm -rf "webapps/zbo.war"'
			}
			sh "ln -s 'zbo-dir/2.8.0/zbo.war' '/var/lib/jetty/webapps/zbo.war'"
			workdir("/var/lib/jetty/bin") {
				sh './jetty.sh start'
			}
		}
    }
}