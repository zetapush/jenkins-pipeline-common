def execute(env = [:]) {
    this.env = env

    node() {

      stage('a') {
          dockerComposeRun(service: 'zms', args: '-u 0:0 -w relative/foo/bar -v relative/toto:relative/toto -v /home/jenkins:/home/jenkins') {
              sh "no-workdir"

              workdir("relative/path") {
                 sh "relative-path"
              }

              withCredentials([usernamePassword(credentialsId: 'jenkins-zp-ux-account', usernameVariable: 'zmsUsername', passwordVariable: 'zmsPassword')]) {
                  workdir("/absolute/path") {
                      sh "absolute-path"
                  }
                  sh "no-workdir in credentials"
              }

              sh "no-workdir"
          }
      }
    }
}