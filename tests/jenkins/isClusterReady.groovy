def execute(env = [:]) {
	this.env = env


	node() {
		def cl = isClusterReadyAndUptodate.asClosure()
		cl.delegate = delegate
		
		stage('a') {
			return cl(
				server: 'dev',
				requiredVersion: '2',
				strs: '*'
			)
		}
	}
}