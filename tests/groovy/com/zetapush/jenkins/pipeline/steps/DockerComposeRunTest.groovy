package com.zetapush.jenkins.pipeline.steps

import static com.lesfurets.jenkins.unit.global.lib.LibraryConfiguration.library
import static com.lesfurets.jenkins.unit.global.lib.LocalSource.localSource

import com.lesfurets.jenkins.unit.MethodSignature
import com.zetapush.jenkins.pipeline.BasePipelineSpecification

class DockerComposeRunTest extends BasePipelineSpecification {
	Closure sh = Mock()
	Closure usernamePassword = Mock()


	void setup() {
		usernamePassword.call(_) >> 'zmsUsername'
		registerAllowedMethod(MethodSignature.method("usernamePassword", Map.class), usernamePassword)
		registerAllowedMethod(MethodSignature.method("sh", String.class), sh)
		registerAllowedMethod(MethodSignature.method("sh", Map.class), sh)
		registerAllowedMethod(MethodSignature.method("echo", String.class), debug)
	}

	def "docker compose run with credentials should execute sub-tasks in docker context" () {
		given:
			def composeCmd = "docker-compose run -v null:null:rw,z -w null -u 0:0 zms"
			def script = loadScript("dockerComposeRunWithCredentials.groovy")

		when:
			script.execute([DEBUG_SCRIPTS_COMMANDS: 'true'])

		then:
			with(sh) {
				// dockerComposeRun(service: 'zms', args: '-u 0:0') {
				//	sh "echo 'dockerComposeRun:zms:sh-1'"
				1 * call(composeCmd+" echo 'dockerComposeRun:zms:sh-1'")
				//	debugEnv(name: 'dockerComposeRun:zms:debug')
				1 * call(composeCmd+" hostname")
				1 * call(composeCmd+" uname -a")
				1 * call(composeCmd+" pwd")
				1 * call(composeCmd+" id")
				1 * call(composeCmd+" docker --version")
				//  withCredentials([usernamePassword(credentialsId: 'jenkins-zp-ux-account', usernameVariable: 'zmsUsername', passwordVariable: 'zmsPassword')]) {
				// 		sh "echo 'dockerComposeRun:withCredentials:sh-1'"
				1 * call(composeCmd+" echo 'dockerComposeRun:withCredentials:sh-1'")
				//		sh "echo 'dockerComposeRun:withCredentials:sh-2'"
				1 * call(composeCmd+" echo 'dockerComposeRun:withCredentials:sh-2'")
				//		debugEnv(name: 'dockerComposeRun:zms:withCredentials:debug')
				1 * call(composeCmd+" hostname")
				1 * call(composeCmd+" uname -a")
				1 * call(composeCmd+" pwd")
				1 * call(composeCmd+" id")
				1 * call(composeCmd+" docker --version")
				//	}
				//	sh "echo 'dockerComposeRun:zms:sh-2'"
				1 * call(composeCmd+" echo 'dockerComposeRun:zms:sh-2'")
				// }
			}
	}

	def "docker compose run with exception ignored should not fail" () {
		given:
			def composeCmd = "docker-compose run -v null:null:rw,z -w null -u 0:0 zms"
			def script = loadScript("dockerComposeRunWithCredentials.groovy")

		when:
			script.execute([DEBUG_SCRIPTS_COMMANDS: 'true'])

		then:
			// dockerComposeRun(service: 'zms', args: '-u 0:0') {
			//	sh "echo 'dockerComposeRun:zms:sh-1'"
			1 * sh.call(composeCmd+" echo 'dockerComposeRun:zms:sh-1'")
			//	debugEnv(name: 'dockerComposeRun:zms:debug')
			1 * sh.call(composeCmd+" hostname") 			>> { throw new Exception('hostname') }
			1 * sh.call(composeCmd+" uname -a") 			>> { throw new Exception('uname-a') }
			1 * sh.call(composeCmd+" pwd") 					>> { throw new Exception('pwd') }
			1 * sh.call(composeCmd+" id") 					>> { throw new Exception('id') }
			1 * sh.call(composeCmd+" docker --version") 	>> { throw new Exception('docker --version') }
			//  withCredentials([usernamePassword(credentialsId: 'jenkins-zp-ux-account', usernameVariable: 'zmsUsername', passwordVariable: 'zmsPassword')]) {
			// 		sh "echo 'dockerComposeRun:withCredentials:sh-1'"
			1 * sh.call(composeCmd+" echo 'dockerComposeRun:withCredentials:sh-1'")
			//		sh "echo 'dockerComposeRun:withCredentials:sh-2'"
			1 * sh.call(composeCmd+" echo 'dockerComposeRun:withCredentials:sh-2'")
			//		debugEnv(name: 'dockerComposeRun:zms:withCredentials:debug')
			1 * sh.call(composeCmd+" hostname") 			>> { throw new Exception('hostname') }
			1 * sh.call(composeCmd+" uname -a") 			>> { throw new Exception('uname-a') }
			1 * sh.call(composeCmd+" pwd") 					>> { throw new Exception('pwd') }
			1 * sh.call(composeCmd+" id") 					>> { throw new Exception('id') }
			1 * sh.call(composeCmd+" docker --version") 	>> { throw new Exception('docker --version') }
			//	}
			//	sh "echo 'dockerComposeRun:zms:sh-2'"
			1 * sh.call(composeCmd+" echo 'dockerComposeRun:zms:sh-2'")
			// }
	}

	def "docker compose run using workdir()" () {
		given:
			def composeCmd = { args -> "docker-compose run -v /data/jenkins:/data/jenkins:rw,z ${args} zms" }
			def script = loadScript("dockerComposeRunWorkdir.groovy")

		when:
			script.execute([DEBUG_SCRIPTS_COMMANDS: 'true', WORKSPACE: '/data/jenkins'])

		then:
			with(sh) {
				1 * call(composeCmd('-w /data/jenkins -u 0:0')+" no-workdir")
				1 * call(composeCmd('-w /data/jenkins/relative/path -u 0:0')+" relative-path")
				1 * call(composeCmd('-w /absolute/path -u 0:0')+" absolute-path")
				1 * call(composeCmd('-w /data/jenkins -u 0:0')+" no-workdir in credentials")
				1 * call(composeCmd('-w /data/jenkins -u 0:0')+" no-workdir")
			}
	}


	def "docker compose run using workdir() with custom args" () {
		given:
			def composeCmd = { args -> "docker-compose run -v /data/jenkins:/data/jenkins:rw,z ${args} zms" }
			def script = loadScript("dockerComposeRunWorkdirWithPathsInArgs.groovy")

		when:
			script.execute([DEBUG_SCRIPTS_COMMANDS: 'true', WORKSPACE: '/data/jenkins'])

		then:
			with(sh) {
				1 * call(composeCmd('-w /data/jenkins -u 0:0 -w /data/jenkins/relative/foo/bar -v /data/jenkins/relative/toto:relative/toto -v /home/jenkins:/home/jenkins')+" no-workdir")
				1 * call(composeCmd('-w /data/jenkins/relative/path -u 0:0 -w /data/jenkins/relative/foo/bar -v /data/jenkins/relative/toto:relative/toto -v /home/jenkins:/home/jenkins')+" relative-path")
				1 * call(composeCmd('-w /absolute/path -u 0:0 -w /data/jenkins/relative/foo/bar -v /data/jenkins/relative/toto:relative/toto -v /home/jenkins:/home/jenkins')+" absolute-path")
				1 * call(composeCmd('-w /data/jenkins -u 0:0 -w /data/jenkins/relative/foo/bar -v /data/jenkins/relative/toto:relative/toto -v /home/jenkins:/home/jenkins')+" no-workdir in credentials")
				1 * call(composeCmd('-w /data/jenkins -u 0:0 -w /data/jenkins/relative/foo/bar -v /data/jenkins/relative/toto:relative/toto -v /home/jenkins:/home/jenkins')+" no-workdir")
			}
	}


	def "docker compose run using file()" () {
		given:
			def composeCmd = { args -> "docker-compose run -v /data/jenkins:/data/jenkins:rw,z${args ? ' '+args : ''} -u 0:0 zms" }
			def script = loadScript("dockerComposeRunWithFile.groovy")

		when:
			script.execute([DEBUG_SCRIPTS_COMMANDS: 'true', WORKSPACE: '/data/jenkins'])

		then:
			// dockerComposeRun(service: 'zms', args: '-u 0:0') {
			// 	file(path: 'com.zetapush.ci/in-docker') {
			// 		exists ?
			1 * sh.call([
					script: composeCmd('-w /data/jenkins')+" test -f com.zetapush.ci/in-docker && echo 'true' || echo 'false'",
					returnStdout: true]) >> 'true'
			0 * sh.call(composeCmd('-w /data/jenkins')+" mkdir -p com.zetapush.ci")
			0 * sh.call(composeCmd('-w /data/jenkins')+" "+FileTest.toNewLineCommand('com.zetapush.ci/in-docker'))
			// 		append new line if first append
			1 * sh.call(composeCmd('-w /data/jenkins')+" "+FileTest.toNewLineCommand('com.zetapush.ci/in-docker', true))
			// 		append lines
			1 * sh.call(composeCmd('-w /data/jenkins')+" "+FileTest.toWriteCommand('# specific username/password for Jenkins account', 'com.zetapush.ci/in-docker'))
			1 * sh.call(composeCmd('-w /data/jenkins')+" "+FileTest.toWriteCommand('zms.username=zmsUsername', 'com.zetapush.ci/in-docker'))
			1 * sh.call(composeCmd('-w /data/jenkins')+" "+FileTest.toWriteCommand('zms.password=zmsPassword', 'com.zetapush.ci/in-docker'))
			//	}
			//
			//	workdir("relative/path") {
			//		file(path: 'com.zetapush.ci/in-docker-workdir') {
			// 			exists ?
			1 * sh.call([
					script: composeCmd('-w /data/jenkins/relative/path')+" test -f com.zetapush.ci/in-docker-workdir && echo 'true' || echo 'false'",
					returnStdout: true]) >> 'true'
			0 * sh.call(composeCmd('-w /data/jenkins/relative/path')+" mkdir -p com.zetapush.ci")
			0 * sh.call(composeCmd('-w /data/jenkins/relative/path')+" "+FileTest.toNewLineCommand('com.zetapush.ci/in-docker-workdir'))
			// 			append new line if first append
			1 * sh.call(composeCmd('-w /data/jenkins/relative/path')+" "+FileTest.toNewLineCommand('com.zetapush.ci/in-docker-workdir', true))
			// 			append lines
			1 * sh.call(composeCmd('-w /data/jenkins/relative/path')+" "+FileTest.toWriteCommand('# specific username2/password2 for Jenkins account', "com.zetapush.ci/in-docker-workdir"))
			1 * sh.call(composeCmd('-w /data/jenkins/relative/path')+" "+FileTest.toWriteCommand('zms.username2=zmsUsername2', "com.zetapush.ci/in-docker-workdir"))
			1 * sh.call(composeCmd('-w /data/jenkins/relative/path')+" "+FileTest.toWriteCommand('zms.password2=zmsPassword2', "com.zetapush.ci/in-docker-workdir"))
			//		}
			//	}
			// }
			//
			// file(path: 'com.zetapush.ci/out-of-docker') {
			1 * sh.call([
					script: "test -f com.zetapush.ci/out-of-docker && echo 'true' || echo 'false'",
					returnStdout: true]) >> 'true'
			0 * sh.call("mkdir -p com.zetapush.ci")
			0 * sh.call(FileTest.toNewLineCommand("com.zetapush.ci/out-of-docker"))
			// 		append new line if first append
			1 * sh.call(FileTest.toNewLineCommand("com.zetapush.ci/out-of-docker", true))
			// 		append lines
			1 * sh.call(FileTest.toWriteCommand('# specific username3/password3 for Jenkins account', "com.zetapush.ci/out-of-docker"))
			1 * sh.call(FileTest.toWriteCommand('zms.username3=zmsUsername3', "com.zetapush.ci/out-of-docker"))
			1 * sh.call(FileTest.toWriteCommand('zms.password3=zmsPassword3', "com.zetapush.ci/out-of-docker"))
			// }

	}

}
