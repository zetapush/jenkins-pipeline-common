package com.zetapush.jenkins.pipeline.steps

import java.util.concurrent.TimeoutException

import spock.lang.Ignore

import com.lesfurets.jenkins.unit.MethodSignature
import com.zetapush.jenkins.pipeline.BasePipelineSpecification

class IgnoreFailuresTest extends BasePipelineSpecification {
	Closure sh = Mock()
	
	void setup() {
		registerAllowedMethod(MethodSignature.method("echo", String.class), debug)
		registerAllowedMethod(MethodSignature.method("sh", String.class), sh)
		registerAllowedMethod(MethodSignature.method("sh", Map.class), sh)
	}

	
	
	def "fail fast" () {
		given:
			def script = loadScript("ignoreFailuresFirstCommand.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true])

		when:
			script.execute(env)

		then:
			2 * sh.call(_) 	>> "ok" \
							>> { throw new Exception("1") }
	}
	
	def "catch each command" () {
		given:
			def script = loadScript("ignoreAllFailures.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true])

		when:
			script.execute(env)

		then:
			5 * sh.call(_) 	>> "ok" \
							>> { throw new Exception("2") } \
							>> "ok" \
							>> { throw new Exception("4") } \
							>> "ok"
	}
}
