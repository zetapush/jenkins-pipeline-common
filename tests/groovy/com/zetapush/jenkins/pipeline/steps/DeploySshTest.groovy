package com.zetapush.jenkins.pipeline.steps

import groovy.lang.Closure

import com.lesfurets.jenkins.unit.MethodSignature
import com.zetapush.jenkins.pipeline.BasePipelineSpecification

class DeploySshTest extends BasePipelineSpecification {
	Closure sshUserPrivateKey = Mock()
	Closure usernamePassword = Mock()
	
	void setup() {
		usernamePassword.call(_) >> 'zmsUsername'
		registerAllowedMethod(MethodSignature.method("usernamePassword", Map.class), usernamePassword)
		registerAllowedMethod(MethodSignature.method("echo", String.class), debug)
		registerAllowedMethod(MethodSignature.method("sh", String.class), sh)
		registerAllowedMethod(MethodSignature.method("sh", Map.class), sh)
		registerAllowedMethod(MethodSignature.method("sshUserPrivateKey", Map.class), sshUserPrivateKey)
		registerAllowedMethod(MethodSignature.method("sshagent", Map.class, Closure.class), { p, cl -> cl() })
	}


	def "deploy ssh with workdir" () {
		given:
			def script = loadScript("deploySsh.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true])

		when:
			script.execute(env)

		then:
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"mkdir -p zbo-dir/2.8.0\"")
			1 * sh.call("scp -o StrictHostKeyChecking=no  -P 22 -r server-parent/zbo/target/zbo.war toto@zbo-server:zbo-dir/2.8.0")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd '/var/lib/jetty'; bin/jetty.sh stop\"")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd '/var/lib/jetty'; rm -rf \\\"webapps/zbo.war\\\"\"")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"ln -s 'zbo-dir/2.8.0/zbo.war' '/var/lib/jetty/webapps/zbo.war'\"")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd '/var/lib/jetty/bin'; ./jetty.sh start\"")
	}
	
	def "deploy ssh with credentials" () {
		given:
			def script = loadScript("deploySshWithCredentials.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true])

		when:
			script.execute(env)

		then:
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"mkdir -p zbo-dir/2.8.0\"")
			1 * sh.call("scp -o StrictHostKeyChecking=no  -P 22 -r server-parent/zbo/target/zbo.war toto@zbo-server:zbo-dir/2.8.0")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd '/var/lib/jetty'; bin/jetty.sh stop\"")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd '/var/lib/jetty'; rm -rf \\\"webapps/zbo.war\\\"\"")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"ln -s 'zbo-dir/2.8.0/zbo.war' '/var/lib/jetty/webapps/zbo.war'\"")
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd '/var/lib/jetty/bin'; ./jetty.sh start\"")
	}

	def "auto remove old folders" () {
		given:
			def script = loadScript("deploySshAutoRemove.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true])

		when:
			script.execute(env)

		then:
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"mkdir -p zbo-dir/2.8.0\"")
			1 * sh.call("scp -o StrictHostKeyChecking=no  -P 22 -r server-parent/zbo/target/zbo.war toto@zbo-server:zbo-dir/2.8.0")
			1 * sh.call([
				script: "ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd 'zbo-dir'; find -maxdepth 1 -name '*' -printf '%T+ %p\\n' | sort -r | awk '{printf \\\" \\\"\\\$2}'\"", 
				returnStdout: true
			]) >> "./build-android-apk_40 . ./build-android-apk_37 ./master_10 ./develop_152 ./tu-patient_25 ./develop_148 ./tu-patient_19"
			1 * sh.call("ssh -o StrictHostKeyChecking=no  toto@zbo-server -p 22 \"cd 'zbo-dir'; rm -rf ./develop_152 ./tu-patient_25 ./develop_148 ./tu-patient_19\"")
	}
}
