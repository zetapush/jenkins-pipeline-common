package com.zetapush.jenkins.pipeline.steps

import com.lesfurets.jenkins.unit.MethodSignature
import com.zetapush.jenkins.pipeline.BasePipelineSpecification
import org.assertj.core.util.Files

import java.nio.file.Paths

import static com.lesfurets.jenkins.unit.global.lib.LibraryConfiguration.library
import static com.lesfurets.jenkins.unit.global.lib.LocalSource.localSource

class FileTest extends BasePipelineSpecification {
	Closure sh = Mock()

	void setup() {
		registerAllowedMethod(MethodSignature.method("echo", String.class), debug)
		registerAllowedMethod(MethodSignature.method("sh", String.class), sh)
		registerAllowedMethod(MethodSignature.method("sh", Map.class), sh)
	}


	def "append to existing file" () {
		given:
			def script = loadScript("file.groovy")

		when:
			script.execute([WORKSPACE: '/data/jenkins'])

		then:
			// exists ?
			1 * sh.call([script: "test -f com.zetapush.ci/zms.properties && echo 'true' || echo 'false'", returnStdout: true]) >> 'true'
			// create
			0 * sh.call("mkdir -p com.zetapush.ci")
			0 * sh.call(toNewLineCommand("com.zetapush.ci/zms.properties"))
			// append new line if first append
			1 * sh.call(toNewLineCommand("com.zetapush.ci/zms.properties", true))
			// append lines
			1 * sh.call(toWriteCommand('# specific username/password for Jenkins account', 'com.zetapush.ci/zms.properties'))
			1 * sh.call(toWriteCommand('zms.username=zmsUsername', 'com.zetapush.ci/zms.properties'))
			1 * sh.call(toWriteCommand('zms.password=zmsPassword', 'com.zetapush.ci/zms.properties'))
	}

	def "create and append to new file" () {
		given:
			def script = loadScript("file.groovy")

		when:
			script.execute([WORKSPACE: '/data/jenkins'])

		then:
			// exists ?
			1 * sh.call([script: "test -f com.zetapush.ci/zms.properties && echo 'true' || echo 'false'", returnStdout: true]) >> 'false'
			// create
			1 * sh.call("mkdir -p com.zetapush.ci")
			1 * sh.call(toNewLineCommand("com.zetapush.ci/zms.properties"))
			// append new line if first append
			1 * sh.call(toNewLineCommand("com.zetapush.ci/zms.properties", true))
			// append lines
			1 * sh.call(toWriteCommand('# specific username/password for Jenkins account', 'com.zetapush.ci/zms.properties'))
			1 * sh.call(toWriteCommand('zms.username=zmsUsername', 'com.zetapush.ci/zms.properties'))
			1 * sh.call(toWriteCommand('zms.password=zmsPassword', 'com.zetapush.ci/zms.properties'))
	}


	static String toNewLineCommand(String file, boolean append = false) {
		return "sh -c \"echo -e '' ${append ? '>>' : '>'} ${file}\""
	}
	static String toWriteCommand(String line, String file) {
		return "sh -c \"echo -e '${encode(line)}' >> ${file}\""
	}
	static String encode(String s) {
		byte[] utf8 = s.getBytes("UTF-8")
		String encoded = ''
		for(byte b : utf8) {
			encoded += '\\x'+byteToHex(b)
		}
		return encoded
	}

	static String byteToHex(byte b) {
		// Returns hex String representation of byte b
		char[] hexDigit = [
				'0', '1', '2', '3', '4', '5', '6', '7',
				'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
		]
		char[] array = [ hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] ]
		return new String(array);
	}
}
