package com.zetapush.jenkins.pipeline.steps

import spock.lang.Unroll

import com.lesfurets.jenkins.unit.MethodSignature
import com.zetapush.jenkins.pipeline.BasePipelineSpecification

class isClusterReadyAndUptodateTest extends BasePipelineSpecification {
	Closure httpRequest = Mock()
	Closure zboAuthenticate = Mock()

	void setup() {
		registerAllowedMethod(MethodSignature.method("echo", String.class), debug)
		registerAllowedMethod(MethodSignature.method("sh", String.class), debug)
		registerAllowedMethod(MethodSignature.method("sh", Map.class), debug)
		registerAllowedMethod(MethodSignature.method("zboAuthenticate", String.class, String.class), zboAuthenticate)
		registerAllowedMethod(MethodSignature.method("zboAuthenticate", String.class, String.class, Boolean.class), zboAuthenticate)
		registerAllowedMethod(MethodSignature.method("httpRequest", Map.class), httpRequest)
	}


	@Unroll
	def "cluster responds (up=#up, version=#version, strStatus=#strStatus, strVersion=#strVersion, initDone=#initDone)" () {
		given:
			zboAuthenticate.call(*_) >> { ...args -> authResponse() }
			httpRequest.call(*_) >> { ...args -> clusterResponse(200, true, up, version, strStatus, strVersion, initDone) }
			
			def script = loadScript("isClusterReady.groovy")
			env.putAll([
				WORKSPACE: '/data/jenkins',
				DEBUG_SCRIPTS: true,
				ZP_DEV_SERVER_URL: 'http://hq.zpush.io:9080',
				ZP_DEV_ADMIN_CREDENTIALS_ID: 'zbo_dev_admin',
				ZP_DEV_DEFAULT_CLUSTER_NAME: 'cluster dev'
			])

		expect:
			script.execute(env) == upAndReady

		where:
			initDone | up    | version | strStatus        | strVersion || upAndReady
			true     | true  | '2'     | 'STATUS_RUNNING' | '2'        || true
			true     | true  | '2'     | 'STATUS_RUNNING' | '0'        || false
			true     | true  | '2'     | 'STATUS_WAITING' | '2'        || false
			true     | true  | '2'     | 'STATUS_WAITING' | '0'        || false
			true     | true  | '0'     | 'STATUS_RUNNING' | '2'        || false
			true     | true  | '0'     | 'STATUS_RUNNING' | '0'        || false
			true     | true  | '0'     | 'STATUS_WAITING' | '2'        || false
			true     | true  | '0'     | 'STATUS_WAITING' | '0'        || false
			true     | false | '2'     | 'STATUS_RUNNING' | '2'        || true
			true     | false | '2'     | 'STATUS_RUNNING' | '0'        || false
			true     | false | '2'     | 'STATUS_WAITING' | '2'        || false
			true     | false | '2'     | 'STATUS_WAITING' | '2'        || false
			true     | false | '0'     | 'STATUS_RUNNING' | '2'        || false
			true     | false | '0'     | 'STATUS_RUNNING' | '0'        || false
			true     | false | '0'     | 'STATUS_WAITING' | '2'        || false
			true     | false | '0'     | 'STATUS_WAITING' | '0'        || false
			false    | true  | '2'     | 'STATUS_RUNNING' | '2'        || false
			false    | true  | '2'     | 'STATUS_RUNNING' | '0'        || false
			false    | true  | '2'     | 'STATUS_WAITING' | '2'        || false
			false    | true  | '2'     | 'STATUS_WAITING' | '0'        || false
			false    | true  | '0'     | 'STATUS_RUNNING' | '2'        || false
			false    | true  | '0'     | 'STATUS_RUNNING' | '0'        || false
			false    | true  | '0'     | 'STATUS_WAITING' | '2'        || false
			false    | true  | '0'     | 'STATUS_WAITING' | '0'        || false
			false    | false | '2'     | 'STATUS_RUNNING' | '2'        || false
			false    | false | '2'     | 'STATUS_RUNNING' | '0'        || false
			false    | false | '2'     | 'STATUS_WAITING' | '2'        || false
			false    | false | '2'     | 'STATUS_WAITING' | '2'        || false
			false    | false | '0'     | 'STATUS_RUNNING' | '2'        || false
			false    | false | '0'     | 'STATUS_RUNNING' | '0'        || false
			false    | false | '0'     | 'STATUS_WAITING' | '2'        || false
			false    | false | '0'     | 'STATUS_WAITING' | '0'        || false

	}
	
	
	def authResponse() {
		return [
			info  : [],
			cookies: '',
			authHeaders: [[name: 'Cookie', value: '']]
		]
	}

	def clusterResponse(httpStatus, exists, up, version, strStatus, strVersion, initDone) {
		return [
			status: httpStatus,
			content: [
				"name": "cluster dev",
				"version": version,
				"exists": exists,
				"up": up,
				"strs": [
					"http://vm-str-1:8080/str": [
						"str": "vm-str-1",
						"status": strStatus,
						"version": strVersion,
						"initDone": initDone
					]
				]
			]
		]
	}
}
