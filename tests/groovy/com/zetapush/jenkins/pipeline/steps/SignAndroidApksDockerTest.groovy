package com.zetapush.jenkins.pipeline.steps

import spock.lang.Ignore

import com.lesfurets.jenkins.unit.MethodSignature
import com.zetapush.jenkins.pipeline.BasePipelineSpecification

class SignAndroidApksDockerTest extends BasePipelineSpecification {
	Closure sh = Mock()
	Closure certificate = Mock()
	Closure signAndroidApks = Mock()
	Closure findFiles = Mock()
	FileWrapper file = Mock()
	
	void setup() {
		registerAllowedMethod(MethodSignature.method("echo", String.class), debug)
		registerAllowedMethod(MethodSignature.method("sh", String.class), sh)
		registerAllowedMethod(MethodSignature.method("sh", Map.class), sh)
		registerAllowedMethod(MethodSignature.method("certificate", Map.class), certificate)
		registerAllowedMethod(MethodSignature.method("withCredentials", List.class, Closure.class), { list, closure -> 
			setVariables([
				APK_SIGN_KEYSTORE: 'store-path', 
				APK_SIGN_KEYSTORE_ALIAS: 'alias-test',
				APK_SIGN_KEYSTORE_PASSWORD: 'store-password'
			])
			def result = closure.call()
			setVariables([
				APK_SIGN_KEYSTORE: null,
				APK_SIGN_KEYSTORE_ALIAS: null,
				APK_SIGN_KEYSTORE_PASSWORD: null
			])
			return result
		})
		registerAllowedMethod(MethodSignature.method("signAndroidApks", Map.class), signAndroidApks)
		registerAllowedMethod(MethodSignature.method("findFiles", Map.class), findFiles)
		file.getName() >> 'android-release-unsigned.apk'
		file.isDirectory() >> false
		file.getPath() >> 'platforms/android/build/outputs/apk/android-release-unsigned.apk'
		findFiles.call(_) >> [file]
		sh.call({ it.script.contains('find') } as Map) >> './platforms/android/build/outputs/apk/android-release-unsigned.apk'
	}


	def "sign apk from jenkins" () {
		given:
			def script = loadScript("signApk.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true])

		when:
			script.execute(env)

		then:
			1 * sh.call("rm -f 'platforms/android/build/outputs/apk/android-release-signed.apk'")
			1 * sh.call("mkdir -p 'platforms/android/build/outputs/apk'")
			1 * sh.call("jarsigner -verbose " +
							"-sigalg SHA1withRSA " +
							"-digestalg SHA1 " +
							"-keystore 'store-path' " +
							"-storepass 'store-password' " +
							"-signedjar 'platforms/android/build/outputs/apk/android-release-signed.apk' " +
							"'platforms/android/build/outputs/apk/android-release-unsigned.apk' " +
							"'alias-test'")
			1 * sh.call("rm -f 'platforms/android/build/outputs/apk/android-release-aligned.apk'")
			1 * sh.call("mkdir -p 'platforms/android/build/outputs/apk'")
			1 * sh.call("zipalign -f -p 4 'platforms/android/build/outputs/apk/android-release-signed.apk' 'platforms/android/build/outputs/apk/android-release-aligned.apk'")
	}

	def "sign apk in docker" () {
		given:
			def script = loadScript("signApkInDocker.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true, DEBUG_SCRIPTS_COMMANDS: true])
			def composeCmd = { cmd -> "docker-compose run -v /data/jenkins:/data/jenkins:rw,z -w /data/jenkins -u 0:0 zms ${cmd}"}

		when:
			script.execute(env)

		then:
			1 * sh.call(composeCmd("rm -f 'platforms/android/build/outputs/apk/android-release-signed.apk'"))
			1 * sh.call(composeCmd("mkdir -p 'platforms/android/build/outputs/apk'"))
			1 * sh.call(composeCmd("jarsigner -verbose " +
							"-sigalg SHA1withRSA " +
							"-digestalg SHA1 " +
							"-keystore 'store-path' " +
							"-storepass 'store-password' " +
							"-signedjar 'platforms/android/build/outputs/apk/android-release-signed.apk' " +
							"'platforms/android/build/outputs/apk/android-release-unsigned.apk' " +
							"'alias-test'"))
			1 * sh.call(composeCmd("rm -f 'platforms/android/build/outputs/apk/android-release-aligned.apk'"))
			1 * sh.call(composeCmd("mkdir -p 'platforms/android/build/outputs/apk'"))
			1 * sh.call(composeCmd("zipalign -f -p 4 'platforms/android/build/outputs/apk/android-release-signed.apk' 'platforms/android/build/outputs/apk/android-release-aligned.apk'"))
	}


	interface FileWrapper {
		String getName()
		boolean isDirectory()
		String getPath()
	}
}
