package com.zetapush.jenkins.pipeline.steps

import java.util.concurrent.TimeoutException

import spock.lang.Ignore

import com.lesfurets.jenkins.unit.MethodSignature
import com.zetapush.jenkins.pipeline.BasePipelineSpecification

class KillTest extends BasePipelineSpecification {
	Closure sh = Mock()
	Closure waitUntil = Mock()
	
	void setup() {
		registerAllowedMethod(MethodSignature.method("echo", String.class), debug)
		registerAllowedMethod(MethodSignature.method("sh", String.class), sh)
		registerAllowedMethod(MethodSignature.method("sh", Map.class), sh)
		registerAllowedMethod(MethodSignature.method("waitUntil", Closure.class), waitUntil)
		waitUntil.call(_) >> { args -> 
			// TODO: why ??????????????
			def cl = args[0][0]
			def i=0
			while(i++<3) {
				if(cl()) {
					return;
				}
			}
			throw new TimeoutException("mock")
		}
	}


	def "kill thumbnailers" () {
		given:
			def script = loadScript("killThumbnailers.groovy")
			env.putAll([WORKSPACE: '/data/jenkins', DEBUG_SCRIPTS: true])

		when:
			script.execute(env)

		then:
			1 * sh.call([script: "ps -aux", returnStdout: true]) >> psResult
			// kill first (termination ok after 3 tries)
			1 * sh.call([script: "kill 11", returnStatus: true]) >> 0
			3 * sh.call([script: "ps -p 11 --no-header", returnStatus: true]) >> 1 >> 1 >> 0
			// kill second (termination ko)
			1 * sh.call([script: "kill 17", returnStatus: true]) >> 1
			3 * sh.call([script: "ps -p 17 --no-header", returnStatus: true]) >> 1
			// kill second (force kill)
			1 * sh.call([script: "kill -9 17", returnStatus: true]) >> 0
			2 * sh.call([script: "ps -p 17 --no-header", returnStatus: true]) >> 1 >> 0
	}
	
	
	def psResult = """USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
					 |root         1  0.0  0.0  33360  2888 ?        Ss   janv.24   0:14 /sbin/init
					 |root         2  0.0  0.0      0     0 ?        S    janv.24   0:01 [kthreadd]
					 |root         3  0.0  0.0      0     0 ?        S    janv.24   4:58 [ksoftirqd/0]
					 |root         5  0.0  0.0      0     0 ?        S<   janv.24   0:00 [kworker/0:0H]
					 |root         7  0.4  0.0      0     0 ?        S    janv.24 284:31 [rcu_sched]
					 |root         8  0.0  0.0      0     0 ?        S    janv.24  14:53 [rcuos/0]
					 |root         9  0.0  0.0      0     0 ?        S    janv.24  11:40 [rcuos/1]
					 |root        10  0.0  0.0      0     0 ?        S    janv.24  10:02 [rcuos/2]
					 |root        11  0.0  0.0      0     0 ?        S    janv.24   9:13 /usr/bin/java -cp tasker-2.8.0-with-dependencies.jar com.zetapush.tasker.Thumbnailer -u tasker -p fdgjqs -m 10010 
					 |root        12  0.1  0.0      0     0 ?        S    janv.24  79:03 [rcuos/4]
					 |root        13  0.0  0.0      0     0 ?        S    janv.24  16:30 [rcuos/5]
					 |root        14  0.0  0.0      0     0 ?        S    janv.24   0:00 [rcu_bh]
					 |root        15  0.0  0.0      0     0 ?        S    janv.24   0:00 [rcuob/0]
					 |root        16  0.0  0.0      0     0 ?        S    janv.24   0:00 [rcuob/1]
					 |root        17  0.0  0.0      0     0 ?        S    janv.24   0:00 /usr/bin/java -cp tasker-2.8.1-with-dependencies.jar com.zetapush.tasker.Thumbnailer -u tasker -p fdgjqs -m 10010
					 |root        18  0.0  0.0      0     0 ?        S    janv.24   0:00 [rcuob/3]
					""".stripMargin('|')
}
