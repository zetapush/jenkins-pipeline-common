package com.zetapush.jenkins.pipeline

import com.lesfurets.jenkins.unit.BasePipelineTest
import com.lesfurets.jenkins.unit.MethodSignature
import com.lesfurets.jenkins.unit.global.lib.LibraryConfiguration

import spock.lang.Specification

import java.util.Map
import java.util.function.Consumer
import java.util.function.Function

import static com.lesfurets.jenkins.unit.MethodSignature.method
import static com.lesfurets.jenkins.unit.global.lib.LibraryConfiguration.library
import static com.lesfurets.jenkins.unit.global.lib.LocalSource.localSource

abstract class BasePipelineSpecification extends Specification {
    protected Closure debug = { s -> System.out.println(s); return s }
    protected Closure sh = Mock()
    protected wrapper = new PipelineTestWrapper()
	protected Map env = new LinkedHashMap()
	
    void setup() {
		def library = library()
						.name('jenkins-pipeline-common')
						.retriever(localSource('tests/libs'))
						.targetPath('jenkins-pipeline-common@master')
						.defaultVersion("master")
						.allowOverride(true)
						.implicit(true)
						.build()
		registerSharedLibrary(library)

        wrapper.setUp()
    }

	
	void setVariable(String name, Object value) {
		env.put(name, value)
		wrapper.binding.setVariable(name, value)
	}

	void setVariables(Map variables) {
		for(def entry : variables.entrySet()) {
			setVariable(entry.key, entry.value)
		}
	}

    /**
     * @param name method name
     * @param args parameter types
     * @param closure method implementation, can be null
     */
    void registerAllowedMethod(String name, List<Class> args = [], Closure closure) {
        wrapper.helper.registerAllowedMethod(name, args, closure)
    }

    /**
     * Register a callback implementation for a method
     * Calls from the loaded scripts to allowed methods will call the given implementation
     * Null callbacks will only log the call and do nothing
     * @param methodSignature method signature
     * @param closure method implementation, can be null
     */
    void registerAllowedMethod(MethodSignature methodSignature, Closure closure) {
        wrapper.helper.registerAllowedMethod(methodSignature, closure)
    }

    /**
     *
     * @param methodSignature
     * @param callback
     */
    void registerAllowedMethod(MethodSignature methodSignature, Function callback) {
        wrapper.helper.registerAllowedMethod(methodSignature, callback)
    }

    /**
     *
     * @param methodSignature
     * @param callback
     */
    void registerAllowedMethod(MethodSignature methodSignature, Consumer callback) {
        wrapper.helper.registerAllowedMethod(methodSignature, callback)
    }

    /**
     * Register library description
     * See {@link com.lesfurets.jenkins.unit.global.lib.LibraryConfiguration} for its description
     * @param libraryDescription to add
     */
    void registerSharedLibrary(LibraryConfiguration libraryDescription) {
        wrapper.helper.libraries.put(libraryDescription.name, libraryDescription)
    }

    /**
     * Clear call stack
     */
    void clearCallStack() {
        wrapper.helper.clearCallStack()
    }

    /**
     * Count the number of calls to the method with name
     * @param name method name
     * @return call number
     */
    long methodCallCount(String name) {
        wrapper.helper.methodCallCount(name)
    }


    class PipelineTestWrapper extends BasePipelineTest {

    }

    /**
     * Updates the build status.
     * Can be useful when mocking a jenkins method.
     * @param status job status to set
     */
    void updateBuildStatus(String status) {
        wrapper.updateBuildStatus(status)
    }

    /**
     * Loads without running the script by its name/path, returning the Script
     * @param scriptName script name or path
     * @return script object
     */
    Script loadScript(String scriptName) {
        return wrapper.loadScript('tests/jenkins/'+scriptName)
    }

    /**
     * Loads and runs the script by its name/path
     * @param scriptName script name or path
     * @return the return value of the script
     */
    Object runScript(String scriptName) {
        return wrapper.runScript(scriptName)
    }

    /**
     * Run the script object
     * @param script Script object
     * @return the return value of the script
     */
    Object runScript(Script script) {
        return wrapper.runScript(script)
    }

    void printCallStack() {
        wrapper.printCallStack()
    }

    /**
     * Asserts the job status is FAILURE.
     * Please check the mocks update this status when necessary.
     * @See # updateBuildStatus ( String )
     */
    void assertJobStatusFailure() {
        wrapper.assertJobStatusFailure()
    }

    /**
     * Asserts the job status is UNSTABLE.
     * Please check the mocks update this status when necessary
     * @See # updateBuildStatus ( String )
     */
    void assertJobStatusUnstable() {
        wrapper.assertJobStatusUnstable()
    }

    /**
     * Asserts the job status is SUCCESS.
     * Please check the mocks update this status when necessary
     * @See # updateBuildStatus ( String )
     */
    void assertJobStatusSuccess() {
        wrapper.assertJobStatusSuccess()
    }

}
