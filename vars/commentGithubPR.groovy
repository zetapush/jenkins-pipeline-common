def call(Map params = [:]) {
  params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
  def comment = params.comment
	def credentialsId = params.credentials
  def repositoryUrl = env.GIT_URL
  def repositoryName = repositoryUrl.replace("git@github.com:","").replace("https://github.com/","").replace(".git","")
  def pullRequestId = env.CHANGE_ID

  withCredentials([string(credentialsId: credentialsId, variable: 'GITHUB_TOKEN')]) {
    try {
      httpRequest(
        url: "https://api.github.com/repos/${repositoryName}/issues/${pullRequestId}/comments",
        httpMode: 'POST',
        customHeaders: [[
          name: 'Authorization',
          value: "token ${GITHUB_TOKEN}"
        ]],
        quiet: !params.debugMode,
        consoleLogResponseBody: params.debugMode,
        contentType: 'APPLICATION_JSON',
        requestBody: """{
          "body": "${comment.replaceAll('"', '\\"')}"
        }"""
      )
    } catch(e) {
			echo "Failed to comment pull request. Cause: ${e.message}"
			if(params.debugMode) {
				echo e.toString()
			}
      throw e;
    }
  }
}