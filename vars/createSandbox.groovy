import groovy.json.JsonSlurperClassic


def call(Map params, Closure body) {
  params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
  if(params.debugMode) {
    echo "createSandbox(params=${params})"
  }

  String serverUrl = params.serverUrl ?: env["ZP_${params.server.toUpperCase()}_SERVER_URL"]
  String credentials = params.credentials ?: (env["JENKINS_ZP_${params.server.toUpperCase()}_ACCOUNT_CREDENTIALS_ID"] ?: env.JENKINS_ZP_ACCOUNT_CREDENTIALS_ID)

  if(params.debugMode) {
    echo "Server ${serverUrl} with credentials ${credentials}"
  }

  def alias = params.alias
  def name = params.name + ' - ' + new Date().format("YYYY-MM-dd hh:mm:ss")
  def description = params.description ?: ''
  def clusterName = params.clusterName
  def clusterId = params.clusterId ?: null
  def packId = params.packId ?: null


  def user = zboAuthenticate(serverUrl, credentials, params.debugMode)
  if(clusterId==null) {
    clusterId = getZetapushClusterId(serverUrl, user, clusterName, params.debugMode)
  }
  if(params.debugMode) {
    echo "serverUrl=${serverUrl}"
    echo "user=${user}"
    echo "alias=${alias}"
    echo "name=${name}"
    echo "description=${description}"
    echo "clusterId=${clusterId}"
    echo "packId=${packId}"
  }
  def businessId = createZetapushSandbox(serverUrl, user, alias, name, description, clusterId, packId, params.debugMode)
  echo "new created sandbox: ${alias} -> ${businessId}"

  if(body) {
    def context = body.delegate
    body.delegate = new CreateSandboxDelegate(params, env, context)
    body.resolveStrategy = Closure.DELEGATE_ONLY
    body()
  }
}

class CreateSandboxDelegate {
  private Map params
	private Object env
	private Object context

	CreateSandboxDelegate(Map params, Object env, Object context) {
		this.params = params
		this.env = env
		this.context = context
	}

  void sandboxLimits(Map params) {
    def merged = this.params + params
    context.sandboxLimits(merged)
  }
}


Long getZetapushClusterId(String url, Object user, String cluster, debugMode) {
  try {
    def response = httpRequest(
      url: "${url}/zbo/orga/cluster/list",
      contentType: 'APPLICATION_JSON',
      httpMode: 'GET',
      customHeaders: user.authHeaders,
      quiet: !debugMode
    )
    def list = new JsonSlurperClassic().parseText(response.content)
    for(c in list) {
      if(cluster==null && c.free) {
        return c.id
      }
      if(cluster!=null && cluster==c.name) {
        return c.id
      }
    }
  } catch(e) {
    if(debugMode) {
        echo "[WARNING] Can't get cluster id. Using default one (id=1)"
    }
  }
  // default cluster id
  return 1L
}

def createZetapushSandbox(String url, Object user, String alias, String name, String desc, cluster, packId, debugMode) {
  def response = httpRequest(
    url: "${url}/zbo/orga/business/create",
    contentType: 'APPLICATION_JSON',
    httpMode: 'POST',
    customHeaders: user.authHeaders,
    quiet: !debugMode,
    requestBody: """{
      "userId": ${user.info.userId},
      "orgaId": ${user.info.orgaId},
      "name": "${name}",
      "description": "${desc}",
      "cluster": ${cluster},
      ${packId!=null ? '"pack": '+packId+',' : ''}
      "aliases": ["${alias}"]
    }"""
  )
  return new JsonSlurperClassic().parseText(response.content).businessId
}