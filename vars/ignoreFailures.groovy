import groovy.lang.Closure

import java.util.Map

import com.zetapush.jenkins.pipeline.StandardMethodsDelegate

def call(Map params = [:], Closure body) {
	def helper = new IgnoreFailureHelper()
	if(params.failFirstError) {
		try {
			return body()
		} catch(Exception e) {
			helper.handleCaughtException(params, body.owner, e)
		}
	} else {
		body.delegate = new IgnoreFailuresDelegate(body.delegate, params, helper)
		body.resolveStrategy = Closure.DELEGATE_FIRST
		return body()
	}
}

class IgnoreFailureHelper {
	def handleCaughtException(Map params, Object context, Exception e) {
		context.echo "Failed to execute action. Continue anyway"
		context.echo "Cause: ${e.getMessage()}"
		if(params.failureStatus) {
			context.currentBuild.result = params.failureStatus
		}
	}
}


class IgnoreFailuresDelegate extends StandardMethodsDelegate {
	Object context
	Map params
	IgnoreFailureHelper helper
	
	IgnoreFailuresDelegate(context, params, helper) {
		super(context)
		// required since Jenkins rewrite code...
		this.context = context
		this.params = params
		this.helper = helper;
	}
	
	def sh(String cmd) {
		try {
			return context.sh(cmd)
		} catch(Exception e) {
			caught(e)
		}
	}
	
	def sh(Map args) {
		try {
			return context.sh(args)
		} catch(Exception e) {
			caught(e)
		}
	}

	def caught(Exception e) {
		helper.handleCaughtException(params, context, e)
	}
}