# Introduction

Upload an artifact on Nexus repository.
This is the general command. Specialized commands are available:
* nexusApkUpload: upload an APK
* nexusWebAppUpload: zip a folder and upload it to nexus

This command is based on the `nexusArtifactUploader` step provided by Jenkins.
This command wraps the `nexusArtifactUploader` in order to avoid providing the following parameters:
* nexusVersion
* protocol
* nexusUrl
* credentials


# Parameters

## `file`

The physical file to upload.


## `group`

The group identifier of the artifact. For example: `com.zetapush`


## `artifact`

The artifact identifier. For example: `zetapush-ts`

## `version`

The artifact version. For example: `3.0.1`

## `artifactType`

The type of the artifact, can be:
* jar
* zip
* apk
* deb
* ...

## `repository`

The name of the repository that will host the artifact (must be a repository with `hosted` type). 
See http://vm-nexus-1:8082/#browse/browse/components for the whole list


## `credentials` (optional)

The identifier to use for upload to nexus. By default use the environment variable `NEXUS_CREDENTIALS_ID`.


## `nexusUrl` (optional)

The URL of the Nexus repository. By default use the environment variable `NEXUS_URL`



# Examples

```groovy
  nexusUpload(
      file: "plugin-parent/com.zetapush.macroscript.cli/target/zetapush-zms_${zmsVersion}-1_all.deb",
      type: 'deb',
      group: 'cli',
      artifact: 'zetapush-zms',
      version: "${zmsVersion}-1_all",
      repository: 'zms-internal'
  )
```