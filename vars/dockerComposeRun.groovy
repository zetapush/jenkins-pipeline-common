import com.zetapush.jenkins.pipeline.StandardMethodsDelegate

import hudson.FilePath

def call(Map params, Closure body) {
  params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
	params.debugCommand = params.debugCommand ?:  Boolean.valueOf(env["DEBUG_SCRIPTS_COMMANDS"]) ?: false

	def helper = new DockerComposeRunHelper(params)
	def context = body.delegate
	
  if(params.debugMode) {
		context.echo  """
						dockerComposeRun(${params}) {
							${helper.closureCode(body)}
						}

					"""
  }
	
	body.delegate = new DockerComposeRunDelegate(params, null, env, context, body, helper)
	body.resolveStrategy = Closure.DELEGATE_ONLY
	body()
}

class DockerComposeRunDelegate extends StandardMethodsDelegate {
	private Map params
	private String workingdir
	private Object env
	private Object context
	private DockerComposeRunHelper helper
	private Closure originalClosure

	DockerComposeRunDelegate(Map params, String workdir, Object env, Object context, Closure originalClosure, DockerComposeRunHelper helper) {
		super(context)
		this.params = params
		this.env = env
		this.workingdir = workdir
		this.context = context
		this.helper = helper
		this.originalClosure = originalClosure
	}

	def sh(String cmd) {
		if(this.params.debugMode) {
			echo  """
							context=${context.toString()}

							dockerComposeRun(${this.params}) {
								sh(cmd=${cmd})
							}

					  """
		}

//		System.out.println("------------------------------------------------------")
//		System.out.println("docker-compose run " +
//				"-v ${env.WORKSPACE}:${env.WORKSPACE}:rw,z " +
//				"-w ${workingdir ? helper.absolutize(workingdir, env.WORKSPACE) : env.WORKSPACE} " +
//				"${helper.absolutizeArgs(params.args, env.WORKSPACE)} " +
//				"${params.service} " + cmd)
//		System.out.println("------------------------------------------------------")
		if(this.params.debugCommand) {
			echo "docker-compose run " +
					"-v ${env.WORKSPACE}:${env.WORKSPACE}:rw,z " +
					"-w ${workingdir ? helper.absolutize(workingdir, env.WORKSPACE) : env.WORKSPACE} " +
					"${helper.absolutizeArgs(params.args, env.WORKSPACE)} " +
					"${params.service} " + cmd
		}
		return context.sh("docker-compose run " +
					"-v ${env.WORKSPACE}:${env.WORKSPACE}:rw,z " +
					"-w ${workingdir ? helper.absolutize(workingdir, env.WORKSPACE) : env.WORKSPACE} " +
					"${helper.absolutizeArgs(params.args, env.WORKSPACE)} " +
					"${params.service} "+cmd)
	}

	def sh(Map args) {
		if(this.params.debugMode) {
			echo  """
							context=${context.toString()}

							dockerComposeRun(${this.params}) {
								sh(cmd=${args.script})
							}

					  """
		}

		if(this.params.debugCommand) {
			echo "docker-compose run " +
					"-v ${env.WORKSPACE}:${env.WORKSPACE}:rw,z " +
					"-w ${workingdir ? helper.absolutize(workingdir, env.WORKSPACE) : env.WORKSPACE} " +
					"${helper.absolutizeArgs(params.args, env.WORKSPACE)} " +
					"${params.service} " + args.script
		}
		Map newArgs = [:]
		newArgs.putAll(args)
		newArgs.script = "docker-compose run " +
				"-v ${env.WORKSPACE}:${env.WORKSPACE}:rw,z " +
				"-w ${workingdir ? helper.absolutize(workingdir, env.WORKSPACE) : env.WORKSPACE} " +
				"${helper.absolutizeArgs(params.args, env.WORKSPACE)} " +
				"${params.service} "+args.script
		def res = context.sh(newArgs)
		return res
	}

	void ignoreFailures(Map params = [:], Closure cl) {
		cl.delegate = this
		cl.resolveStrategy = Closure.DELEGATE_ONLY
		context.ignoreFailures(params, cl)
	}

	void workdir(String dir, Closure cl) {
		cl.delegate = new DockerComposeRunDelegate(params, dir, env, context, originalClosure, helper)
		cl.resolveStrategy = Closure.DELEGATE_ONLY
		cl()
	}

	def withCredentials(params, Closure body) {
		if(this.params.debugMode) {
			echo  """
							context=${context.toString()}

							dockerComposeRun(${this.params}) {
								withCredentials(params=${params}) {
									${helper.closureCode(body)}
								}
							}

					  """
		}

		body.delegate = this
		body.resolveStrategy = Closure.DELEGATE_ONLY
		return context.withCredentials(params, body)
	}

	void debugEnv(Map params) {
		def cl = context.debugEnv.getDebugEnvClosure()
		cl.delegate = this
		cl.resolveStrategy = Closure.DELEGATE_ONLY
		cl(params)
	}

	void withFile(Map params, Closure cl) {
		cl.delegate = this
		cl.resolveStrategy = Closure.DELEGATE_ONLY
		context.withFile(params, cl)
	}

	List findFiles(Map params) {
		// TODO: improve to match only what Ant Style should match
		def path = params.glob.replaceAll('[*]+', '*')
		def res = context.sh(script: "find -wholename '${path}", returnStdout: true)
		def paths = res.split("(\\r)?\\n")
		List files = []
		for(def p : paths) {
			files.add(new DockerRunFileWrapper(p))
		}
		return files
	}
	
	void signAndroidApks(Map params) {
		context.signAndroidApks(params, this)
	}
}


class DockerComposeRunHelper {
	private Map params
	
	DockerComposeRunHelper(Map params) {
		this.params = params
	}

	def absolutizeArgs(String args, String root) {
		return args.replaceAll('(-[vw] )([^/])', '$1'+root+'/$2')
	}

	def absolutize(String path, String root) {
		return path.startsWith('/') ? path : (root+'/'+path)
	}

	def closureCode(Closure closure) {
		try {
			return closure?.metaClass?.classNode?.getDeclaredMethods("doCall")?.getAt(0)?.code?.text ?: closure.toString()
		} catch(e) {
			return 'unknown body()'
		}
	}
}

class DockerRunFileWrapper {
	private String name
	private boolean directory
	private String path
	
	DockerRunFileWrapper(String path) {
		def file = new FilePath(new File(path))
		this.name = file.name
		this.directory = file.isDirectory()
		this.path = file.getRemote()
	}
}