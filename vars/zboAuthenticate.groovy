import groovy.json.JsonSlurperClassic

def call(String url, String credentials, boolean debugMode = false) {
    if(debugMode) {
        echo "zboAuthenticate(url=${url}, credentials=${credentials})"
    }
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credentials, usernameVariable: 'username', passwordVariable: 'password']]) {
        def response = httpRequest(
                url: "${url}/zbo/auth/login",
                contentType: 'APPLICATION_JSON',
                httpMode: 'POST',
                quiet: !debugMode,
                requestBody: """{
	              "username": "${env.username}",
	              "password": "${env.password}"
	            }"""
        )
        return [
                info  : new JsonSlurperClassic().parseText(response.content),
                cookies: response.headers['Set-Cookie'],
                authHeaders: [[name: 'Cookie', value: response.headers['Set-Cookie'][0]]]   // TODO: Make it work with several cookies
        ]
    }
}
