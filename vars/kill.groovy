import com.zetapush.jenkins.pipeline.matcher.MatcherBuilder


def call(Map params) {
	return asClosure()(params)
}


Closure asClosure() {
	return { params ->
		params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
		def terms = params.terms ?: [params.term]
		def all = params.all ?: false
		def matcher = new MatcherBuilder(terms).build()
		
		String ps = sh(script: 'ps -aux', returnStdout: true)
		if(params.debugMode) {
			echo """processes: 
					|
					|${ps}""".stripMargin('|')
		}
		if(!ps) {
			return
		}
		
		def lines = ps.split("(\\n)?\\n")
		def matchingLines = []
		for(String line : lines) {
			if(matcher.test(line)) {
				matchingLines.add(line)
			}
		}
		if(params.debugMode) {
			echo """matching processes: 
					|
					|${matchingLines.join('\n')}""".stripMargin('|')
		}
		if(!all && matchingLines.size()>1) {
			throw new IllegalStateException("""Several processes match your terms: 
					|${matchingLines.join('\n')}
					|
					| You should:
					| - Either set all parameter to true in order to kill all found instances
					| - Or be more specific on terms""".stripMargin('|'))
		}
		
		for(String line : matchingLines) {
			def cols = line.split('\\s+', 11)
			def pid = cols[1]
			def debugName = "${cols[cols.length-1]} (${pid})"
			if(params.debugMode) {
				echo "killing ${debugName}..."
			}
			try {
				sh(script: "kill ${pid}", returnStatus: true)
				timeout(time: params.graciouslyKillTimeout ?: 5000, unit: 'MILLISECONDS') {
					waitUntil {
						if(params.debugMode) {
							echo "check if ${debugName} still alive"
						}
						sh(script: "ps -p ${pid} --no-header", returnStatus: true)==0
					}
				}
			} catch(Exception e) {
				if(params.debugMode) {
					echo "${e.getMessage()}"
					echo e.toString()
					echo "nice kill of ${debugName} timed out. Force kill"
				}
				sh(script: "kill -9 ${pid}", returnStatus: true)
				timeout(time: params.forceKillTimeout ?: 5000, unit: 'MILLISECONDS') {
					waitUntil {
						if(params.debugMode) {
							echo "check if ${debugName} still alive"
						}
						sh(script: "ps -p ${pid} --no-header", returnStatus: true)==0
					}
				}
			}
			if(params.debugMode) {
				echo "killed ${debugName}"
			}
		}
	}
}

