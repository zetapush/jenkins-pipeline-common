import hudson.FilePath

import com.zetapush.jenkins.pipeline.remote.SshRunHelper
import com.zetapush.jenkins.pipeline.remote.SshRunDelegate


def call(Map params, Closure body = null) {
	def parts = params.server.split(':')
	def server = parts[0]
	def port = parts.length>1 ? parts[1] : 22
	def remoteDirectoryCopy = escapeVariables(params.remoteDirectory)
	def remoteDirectoryExposed = escapeVariables(params.remoteDirectoryExposed)
	def files = params.files
	def credentialsId = params.credentials
	def keep = params.keep
	def sshUsername = null
	
	
	// if server provides its own user in the form user@host
	// then use it. Otherwise load username from credentials
	if(!server.contains("@")) {
		withCredentials([sshUserPrivateKey(credentialsId: credentialsId, 
											usernameVariable: 'sshUsername',
											keyFileVariable: 'dontcareButRequired')]) {
			sshUsername = env.sshUsername
		}
	}
	def helper = new SshRunHelper(server, port, sshUsername, params.extraEnv ?: [:])
	def context = body ? body.delegate : this
	// TODO: handle default behaviors ? Branch/latest ?
	context.sshagent(credentials: [credentialsId]) {
		context.sh helper.sshCommand("mkdir -p ${remoteDirectoryCopy}")
		context.sh helper.scpCommand(files, remoteDirectoryCopy)
		if(remoteDirectoryExposed) {
			context.sh helper.sshCommand("rm -f ${remoteDirectoryExposed}")
			context.sh helper.sshCommand("ln -s ${remoteDirectoryCopy} ${remoteDirectoryExposed}")
		}
		if(body) {
			body.delegate = new SshRunDelegate(params, null, env, context, helper)
			body.resolveStrategy = Closure.DELEGATE_ONLY
			body()
		}
		if(keep) {
			def keepMatchingDir = keep.dir ?: new FilePath(new File(remoteDirectoryCopy)).parent.remote
			def keepMatchingExpr = keep.matching ?: '*'
			def numToKeep = keep.num ?: 50
			def filesToDelete = context.sh(script: helper.sshCommand("cd '${keepMatchingDir}'; find -maxdepth 1 -name '${keepMatchingExpr}' -printf '%T+ %p\\n' | sort -r | awk '{printf \" \"\\\$2}'"), returnStdout: true)
			def f = filesToDelete.replaceAll("(^|\\s+)\\.\\s", " ").trim().split("\\s+")
			if(f.size()>numToKeep) {
				context.sh helper.sshCommand("cd '${keepMatchingDir}'; rm -rf ${f[numToKeep..-1].join(' ')}")
			}
		}
	}
}


def escapeVariables(path) {
	if(!path) {
		return path
	}
	// replace ${BRANCH_NAME} by $( echo ${BRANCH_NAME} | tr '/' '-' )
	return path.replaceAll('(\\$\\{[a-zA-Z0-9_]+\\})', '\\$( echo $1 | tr \'/\' \'-\'})')
}


