def call(Map params) {
	nexusUpload(
		file: params.apk,
		artifact: params.artifact,
		artifactId: params.artifactId,
		type: 'apk',
		group: params.group,
		version: params.version,
		repository: params.repository,
		credentials: params.credentials,
		nexusUrl: params.nexusUrl
	)
}