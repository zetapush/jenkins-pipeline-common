import hudson.FilePath

// TODO: override only if in docker


def call(Map params, Object ctx = null) {
	params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
	
	def context = ctx ?: params.overrideContext ?: this
	
	def mapping = new ApkSiblingMapping(params, env, context)
	if(params.signedApkMapping=='unsignedApkNameDir') {
		mapping = new ApkDirMapping(params, env, context)
	}
	def signer = new ApkSigner(params, env, mapping, context)
	def zipalign = new ZipAlign(params, env, mapping, context)
	
	def handler = new SignApkHandler(params, env, signer, zipalign, context)
	handler.execute()
}


class SignApkHandler {
    private Map params
    private Object env
	private ApkSigner signer
	private ZipAlign zipalign
	private Object context
	
    SignApkHandler(params, env, signer, zipalign, Object context) {
        this.params = params
        this.env = env
		this.signer = signer
		this.zipalign = zipalign
		this.context = context
    }

	def execute() {
		for(Apk unsignedApk : getUnsignedApks()) {
			def signedapk = signer.sign(unsignedApk)
			if(!params.skipZipalign) {
				signedapk = zipalign.align(signedapk)
			}
			if(params.archiveUnsignedApks) {
				archive(unsignedApk)
			}
			if(params.archiveSignedApks) {
				archive(signedapk)
			}
		}
	}

    List<Apk> getUnsignedApks() {
        def files = context.findFiles(glob: params.apksToSign ?: '**/*-unsigned.apk')
        if(params.debugMode) {
            context.echo "found ${files.size()} unsigned apks:"
            for(def file : files) {
                context.echo "   - ${file.path}"
            }
        }
        def apks = []
        for(def file : files) {
            apks.add(new Apk(file))
        }
        return apks
    }

    def archive(Apk apk) {
        context.archiveArtifacts(artifacts: apk.path.remote)
    }
}

interface ApkMapping {
    Apk resolveAlignedDestination(Apk source)
    Apk resolveSignedDestination(Apk source)
}

class ApkSiblingMapping implements ApkMapping {
    private Map params
    private Object env
	private Object context

    ApkSiblingMapping(params, env, Object context) {
        this.params = params
        this.env = env
		this.context = context
    }

    Apk resolveAlignedDestination(Apk source) {
        return new Apk(source, source.path.sibling("${source.appName}-aligned.apk"))
    }

    Apk resolveSignedDestination(Apk source) {
        return new Apk(source, source.path.sibling("${source.appName}-signed.apk"))
    }
}


class ApkDirMapping implements ApkMapping {
    private Map params
    private Object env
	private Object context

    ApkDirMapping(params, env, Object context) {
        this.params = params
        this.env = env
		this.context = context
    }

    Apk resolveAlignedDestination(Apk source) {
        return new Apk(source, source.path.parent.child("${source.originalName}/${source.appName}-aligned.apk"))
    }

    Apk resolveSignedDestination(Apk source) {
        return new Apk(source, source.path.parent.child("${source.originalName}/${source.appName}-signed.apk"))
    }
}


class ApkSigner {
	private Map params
	private Object env
    private ApkMapping mapping
	private Object context
	
    ApkSigner(params, env, ApkMapping mapping, Object context) {
		this.params = params
		this.env = env
        this.mapping = mapping
		this.context = context
    }

    Apk sign(Apk apk) {
        def signedApk = mapping.resolveSignedDestination(apk)
        def signedApkPath = signedApk.path
        context.sh "rm -f '${signedApkPath.remote}'"
        context.sh "mkdir -p '${signedApkPath.parent}'"
        // TODO: handle no alias
		def ctx = context
        context.withCredentials([context.certificate(
                credentialsId: params.keyStoreId,
                keystoreVariable: "APK_SIGN_KEYSTORE",
                aliasVariable: "APK_SIGN_KEYSTORE_ALIAS",
                passwordVariable: "APK_SIGN_KEYSTORE_PASSWORD")]) {
			
			if(params.debugMode) {
//				System.out.println("==================================")
//				System.out.println("keystore")
//				System.out.println("${env.APK_SIGN_KEYSTORE}")
//				System.out.println("storepass")
//				System.out.println("${env.APK_SIGN_KEYSTORE_PASSWORD}")
//				System.out.println("path")
//				System.out.println("${signedApkPath.remote}")
//				System.out.println("alias")
//				System.out.println("${env.APK_SIGN_KEYSTORE_ALIAS}")
//				System.out.println("==================================")
				ctx.echo "jarsigner -verbose " +
							"-sigalg SHA1withRSA " +
							"-digestalg SHA1 " +
							"-keystore '${env.APK_SIGN_KEYSTORE}' " +
							"-storepass '${env.APK_SIGN_KEYSTORE_PASSWORD}' " +
							"-signedjar '${signedApkPath.remote}' " +
							"'${apk.path.remote}' " +
							(params.keyAlias ?: "'${env.APK_SIGN_KEYSTORE_ALIAS}'")
			}
            ctx.sh "jarsigner -verbose " +
							"-sigalg SHA1withRSA " +
							"-digestalg SHA1 " +
							"-keystore '${env.APK_SIGN_KEYSTORE}' " +
							"-storepass '${env.APK_SIGN_KEYSTORE_PASSWORD}' " +
							"-signedjar '${signedApkPath.remote}' " +
							"'${apk.path.remote}' " +
							(params.keyAlias ?: "'${env.APK_SIGN_KEYSTORE_ALIAS}'")
        }
		return signedApk
    }
}

class ZipAlign {
	private Map params
	private Object env
    private String zipalignPath
    private ApkMapping mapping
	private Object context
	
    ZipAlign(params, env, ApkMapping mapping, Object context) {
		this.params = params
		this.env = env
        this.mapping = mapping
		this.context = context
        // TODO: handle ANDROID_HOME and androidHome param
        zipalignPath = params.zipalignPath ?: env.ANDROID_ZIPALIGN ?: 'zipalign'
    }

    Apk align(Apk apk) {
        def alignedApk = mapping.resolveAlignedDestination(apk)
        def alignedApkPath = alignedApk.path
        context.sh "rm -f '${alignedApkPath.remote}'"
        context.sh "mkdir -p '${alignedApkPath.parent}'"
		if(params.debugMode) {
			context.echo "${zipalignPath} -f -p 4 '${apk.path.remote}' '${alignedApkPath.remote}'"
		}
        context.sh "${zipalignPath} -f -p 4 '${apk.path.remote}' '${alignedApkPath.remote}'"
		return alignedApk
    }
}


class Apk {
    FilePath path
    FilePath directory
    String filename
    String originalName
    String appName

    Apk(FilePath path, FilePath directory, String filename) {
        this.path = path
        this.directory = directory
        this.filename = filename
        this.originalName = path.baseName
        this.appName = path.baseName.replaceAll(/(-?[.]?(unsigned|signed|aligned))/, '')
    }
	
	Apk(String path, String directory, String filename) {
		this(new FilePath(new File(path)), new FilePath(new File(directory)), filename)
	}
	
	Apk(Apk source, FilePath path) {
		this(path, path.parent, source.filename)
	}
	
    Apk(file) {
        this(new FilePath(new File(file.path)), new FilePath(new File(file.path)).parent, file.name)
    }
}