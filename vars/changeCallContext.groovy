import java.util.Map

def call(Object globalVar, Object newContext) {
	def method = globalVar.&call
	method.delegate = newContext
	return method
}