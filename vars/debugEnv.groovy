
def getDebugEnvClosure() {
	return { Map params ->
		def name = params.name ?: ''
		ignoreFailures {
			echo "${name}: Hostname"
			sh 'hostname'

			echo "${name}: System"
			sh 'uname -a'

			echo "${name}: Current folder"
			sh 'pwd'

			echo "${name}: Current user"
			sh 'id'

			echo "${name}: in docker?"
			sh 'docker --version'

			echo "${name}: environment variables"
			sh 'printenv'
		}
	}
}


def call(Map params) {
	getDebugEnvClosure()(params)
}
