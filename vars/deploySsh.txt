# Introduction

Use `deploySsh` to copy files from Jenkins workspace to a remote server. Internally, SSH and SCP are used. 

You can specify a directory where to place files on remote server. If the directory doesn't exists, it is created.

You can also ask to create or update a directory that is exposed that is different from previous directory. The previous directory may be on a private directory while the exposed directory is declared in apache static file directory for example. Internally, a symbolic link is created or updated.



# Parameters

## `files`

The SCP expression to indicate which files to copy on remote server. Examples of the SCP syntax:

* `path/to/workspace-directory`: copy `workspace-directory` on the remote server
* `path/to/workspace-directory/*`: copy content of `workspace-directory` on the remote server
* `path/to/workspace-directory/\{file1.txt,file2.txt\}`: copy `file1.txt` and `file2.txt` that are in `workspace-directory` on the remote server

*Environment variables defined in Jenkins can be used.*

## `server`

The remote server address using the form `<host>:<port>`.
The port is the SSH port to machine. If not defined, port 22 is used.

*Environment variables defined in Jenkins can be used.*

## `crendentials`

The identifier of credentials defined in Jenkins configuration.

*Environment variables defined in Jenkins can be used.*

## `remoteDirectory`

The directory where to place files on the remote server.

If directory contains variables, these variables are escaped (all invalid characters are converted to a dash `-`).

*Environment variables defined in Jenkins can be used.*

## `remoteDirectoryExposed` (optional)

Indicates where the uploaded files should be 'duplicated' in order to be visible.
Actually, a symbolic link is created or updated.

If link contains variables, these variables are escaped (all invalid characters are converted to a dash `-`).

*Environment variables defined in Jenkins can be used.*


# Examples

```groovy
deploySsh(
  files: 'static/*',
  credentials: 'credentials-id-for-server-1',
  server: 'server-1:6485',
  remoteDirectory: '/home/joe/my-files/${BRANCH_NAME}_${BUILD_NUMBER}',
  remoteDirectoryExposed: '/var/www/${BRANCH_NAME}'
)
```

For example, we are building the branch `feature/foo` and this is the third build for this branch.

The build copies all files defined in `static` lcoal folder to `server-1` into directory `/home/joe/my-files/feature-foo_3`. The directories `feature` and `foo_3` are automatically created. The files are copied into it.
A symbolic link is created in the folder `/var/www/` with the name `feature-foo`:

`var/www/feature-foo` -> `/home/joe/my-files/feature-foo_3`