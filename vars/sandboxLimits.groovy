import groovy.json.JsonSlurperClassic;



def call(Map params) {
  def ALL_LIMITS = [
    "ES_MAX_DOC_SIZE",
    "GDA_MAX_CELL_SIZE",
    "GDA_MAX_COLUMNS",
    "MACRO_MAX_CONCURRENT",
    "MACRO_MAX_RESOLVES",
    "MACRO_MAX_TIMEMILLIS",
    "MAX_TRIGGERS",
    "RAM_PER_REQUEST"
  ]

  params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
  if(params.debugMode) {
    echo "sanboxLimits(params=${params})"
  }

  def serverUrl = params.serverUrl ?: env["ZP_${params.server.toUpperCase()}_SERVER_URL"]
  def adminCredentials = params.adminCredentials ?: env["ZP_${params.server.toUpperCase()}_ADMIN_CREDENTIALS_ID"]
  // sandboxId or alias
  def sandboxId = params.alias ?: params.sandboxId

  if(params.debugMode) {
    echo "serverUrl=${serverUrl}"
    echo "adminCredentials=${adminCredentials}"
    echo "sandboxId=${sandboxId}"
  }

  def user = zboAuthenticate(serverUrl, adminCredentials, params.debugMode)

  def limits = [:]
  // ALL => relative value: *100
  if(params.all) {
    ALL_LIMITS.each {
      limits[it] = params.all
    }
  } else {
    limits = params.limits
  }
  if(params.debugMode) {
    echo "limits=${limits}"
  }

  // each limit constant (accept value or relative value: +x, -x, *x, /x)
  limits.each { key, value ->
    if(params.debugMode) {
        echo "setLimitValue(${serverUrl}, ${user}, ${sandboxId}, ${key}, ${value})"
    }
    setLimitValue(serverUrl, user, sandboxId, ''+key, value, params)
  }
}


def getLimitValue(String url, Object user, String sandboxId, String limit, Map params) {
  def response = httpRequest(
    url: "${url}/zbo/orga/limit/${sandboxId}/${limit}",
    httpMode: 'GET',
    customHeaders: user.authHeaders,
    quiet: !params.debugMode
  )
  return new JsonSlurperClassic().parseText(response.content)[limit]
}

def setLimitValue(String url, Object user, String sandboxId, String limit, Object value, Map params) {
  def v = value
  if(isRelative(value)) {
    def currentValue = getLimitValue(url, user, sandboxId, limit, params)
    v = Eval.x(currentValue, "x ${value}")
  }
  httpRequest(
    url: "${url}/zbo/orga/limit/override/${sandboxId}/${limit}/${v}",
    httpMode: 'GET',
    customHeaders: user.authHeaders,
    quiet: !params.debugMode
  )
}

def isRelative(value) {
  return value instanceof String && value.matches('^[+-/*].+')
}