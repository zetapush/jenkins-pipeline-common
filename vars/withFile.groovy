import com.zetapush.jenkins.pipeline.file.FileHandler
import com.zetapush.jenkins.pipeline.file.ShFileHandler

def call(Map params, Closure body) {
    params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
    body.delegate = new FilesDelegate(params.path, params, body.delegate, env)
    body.resolveStrategy = Closure.DELEGATE_ONLY
    body()
}





class FilesDelegate implements Serializable {
    private FileHandler handler
    private first = true
    private String encoding
    private boolean appendWithNewLine
    private boolean firstAppendAddsNewLine
    private boolean autoCreate
    private File filePath
    private boolean alreadyExists = false
    def env

    FilesDelegate(String filePath, Map options, Object context, env) {
        this.env = env
        this.filePath = new File(filePath)
        this.encoding = options.getOrDefault('encoding', 'utf-8')
        this.appendWithNewLine = options.getOrDefault('appendWithNewLine', true)
        this.firstAppendAddsNewLine = options.getOrDefault('firstAppendAddsNewLine', true)
        this.autoCreate = options.getOrDefault('autoCreate', true)
        this.handler = options.getOrDefault('handler', new ShFileHandler(encoding, context, env, options.debugMode))
    }

    void create() {
        handler.create(filePath)
    }

    void empty() {
        handler.empty(filePath)
    }

    void append(String content, boolean newLine = true) {
        init()
        handler.append(filePath, content, encoding, newLine)
    }

    void append(List lines, boolean newLineAfter = true) {
        init()
        handler.append(filePath, lines, encoding, newLineAfter)
        if(newLineAfter) {
            this.append('', true)
        }
    }

    private void init() {
        alreadyExists = alreadyExists ?: handler.exists(filePath)
        if(!alreadyExists && autoCreate) {
            alreadyExists = true
            create()
        }
        if(first && firstAppendAddsNewLine) {
            first = false
            append('')
        }
    }
}











