def call(Map params, Closure body) {
	def axes = params.axes;
	script {
		for(def axis : axes) {
			body()
		}
	}
}