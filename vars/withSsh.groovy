import hudson.FilePath

import com.zetapush.jenkins.pipeline.remote.SshRunHelper
import com.zetapush.jenkins.pipeline.remote.SshRunDelegate


def call(Map params, Closure body = null) {
	def parts = params.server.split(':')
	def server = parts[0]
	def port = parts.length>1 ? parts[1] : 22
	def credentialsId = params.credentials
	def sshUsername = null
	
	// if server provides its own user in the form user@host
	// then use it. Otherwise load username from credentials
	if(!server.contains("@")) {
		withCredentials([sshUserPrivateKey(credentialsId: credentialsId, 
											usernameVariable: 'sshUsername',
											keyFileVariable: 'dontcareButRequired')]) {
			sshUsername = env.sshUsername
		}
	}
	def helper = new SshRunHelper(server, port, sshUsername, params.extraEnv ?: [:])
	def context = body ? body.delegate : this
	context.sshagent(credentials: [credentialsId]) {
		if(body) {
			body.delegate = new SshRunDelegate(params, null, env, context, helper)
			body.resolveStrategy = Closure.DELEGATE_ONLY
			body()
		}
	}
}



