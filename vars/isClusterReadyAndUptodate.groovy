import java.util.Map

import com.zetapush.jenkins.pipeline.RequiredArgumentException


def call(Map params) {
	return asClosure()(params)
}

Closure asClosure() {
	return { params ->
		params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
		if(params.debugMode) {
			echo "isClusterReadyAndUptodate(params=${params})"
		}
	
		def requiredVersion = params.requiredVersion
		def strs = params.strs ?: []
		if(!requiredVersion) {
			throw new RequiredArgumentException("requiredVersion")
		}
		
		Closure cs = new getClusterStatus().asClosure()
		cs.delegate = delegate
		def clusterStatus = cs(params)
		
		def zboUptodate = true
		if(!params.skipZbo) {
			zboUptodate = clusterStatus.exists && clusterStatus.version==requiredVersion
			if(params.debugMode) {
				echo "zbo ready ? ${clusterStatus.exists}"
				echo "zbo version ? ${clusterStatus.version}"
				echo "zbo up to date ? ${zboUptodate}"
			}
		}
		def strsUptodate = true
		if(strs instanceof String) {
			strs = clusterStatus.strs.keySet()
		}
		for(def str : strs) {
			def strInfo = clusterStatus.strs[str]
			def strUptodate = strInfo.status=='STATUS_RUNNING' && strInfo.version==requiredVersion && strInfo.initDone
			if(params.debugMode) {
				echo "str ${str} status ? ${strInfo.status}"
				echo "str ${str} version ? ${strInfo.version}"
				echo "str ${str} init done ? ${strInfo.initDone}"
				echo "str ${str} up to date ? ${strUptodate}"
			}
			strsUptodate = strsUptodate && strUptodate
		}
		return zboUptodate && strsUptodate
	}
}