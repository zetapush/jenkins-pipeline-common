import groovy.json.JsonSlurperClassic

import com.zetapush.jenkins.pipeline.RequiredArgumentException

def call(Map params) {
	return asClosure()(params)
}


Closure asClosure() {
	return { params ->
		params.debugMode = params.debug ?: Boolean.valueOf(env["DEBUG_SCRIPTS"]) ?: false
		if(params.debugMode) {
			echo "getClusterStatus(params=${params})"
		}
	
		def serverUrl = params.serverUrl ?: env["ZP_${params.server.toUpperCase()}_SERVER_URL"]
		def adminCredentials = params.adminCredentials ?: env["ZP_${params.server.toUpperCase()}_ADMIN_CREDENTIALS_ID"]
		def clusterName = params.clusterName ?: env["ZP_${params.server.toUpperCase()}_DEFAULT_CLUSTER_NAME"]
		if(!serverUrl) {
			throw new RequiredArgumentException("serverUrl or server", "You should either explicitly provide 'server' or 'serverUrl' parameter or define a new environment variable named 'ZP_${params.server.toUpperCase()}_SERVER_URL' with the URL of ${params.server}")
		}
		if(!adminCredentials) {
			throw new RequiredArgumentException("adminCredentials", "You should either explicitly provide 'adminCredentials' parameter or define a new environment variable named 'ZP_${params.server.toUpperCase()}_ADMIN_CREDENTIALS_ID' with the credentials used to login as admin on ${params.server}")
		}
		if(!clusterName) {
			throw new RequiredArgumentException("clusterName", "You should either explicitly provide 'clusterName' parameter or define a new environment variable named 'ZP_${params.server.toUpperCase()}_DEFAULT_CLUSTER_NAME' with the name of the default cluster on ${params.server}")
		}
		
		if(params.debugMode) {
			echo "serverUrl=${serverUrl}"
			echo "adminCredentials=${adminCredentials}"
			echo "clusterName=${clusterName}"
		}
	
		try {
			def user = zboAuthenticate(serverUrl, adminCredentials, params.debugMode)
			def response = httpRequest(
				url: "${serverUrl}/zbo/adm/cluster/test/${encodeURIComponent(clusterName)}",
				httpMode: 'GET',
				customHeaders: user.authHeaders,
				quiet: !params.debugMode,
				consoleLogResponseBody: params.debugMode
			)
			return response.content instanceof String ? new JsonSlurperClassic().parseText(response.content) : response.content
		} catch(e) {
			echo "Failed to get cluster status. Cause: ${e.message}"
			if(params.debugMode) {
				echo e.toString()
			}
			return [
				name: clusterName, 
				version: null, 
				exists: false,
				up: false,
				strs: []
			]
		}
	}
}

public static String encodeURIComponent(String s)
{
  String result = null;

  try
  {
	result = URLEncoder.encode(s, "UTF-8")
					   .replaceAll("\\+", "%20")
					   .replaceAll("\\%21", "!")
					   .replaceAll("\\%27", "'")
					   .replaceAll("\\%28", "(")
					   .replaceAll("\\%29", ")")
					   .replaceAll("\\%7E", "~");
  }

  // This exception should never occur.
  catch (UnsupportedEncodingException e)
  {
	result = s;
  }

  return result;
}