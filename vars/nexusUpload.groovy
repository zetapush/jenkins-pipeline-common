def call(Map params) {
    def file = params.file
    def group = params.group
    def artifactId = params.artifactId
    def version = params.version
    def type = params.type
    def repository = params.repository
    def credentials = params.credentials ?: env.NEXUS_CREDENTIALS_ID
    def nexusUrl = params.nexusUrl ?: env.NEXUS_URL
    if(params.artifact) {
        def artifactParts = params.artifact.split(':')
        group = artifactParts[0]
        artifactId = artifactParts[1]
        version = artifactParts[2]
        if(artifactParts.length > 3) {
            type = artifactParts[3]
        }
    }
    
    nexusArtifactUploader(
        nexusVersion: 'nexus3',
        protocol: nexusUrl.substring(0, nexusUrl.indexOf('://')),
        nexusUrl: nexusUrl.substring(nexusUrl.indexOf('://')+3).replaceAll("/\$", ""),
        groupId: group,
        version: version,
        repository: repository,
        credentialsId: credentials,
        artifacts: [[
            artifactId: artifactId,
            type: type,
            file: file
        ]]
    )
}