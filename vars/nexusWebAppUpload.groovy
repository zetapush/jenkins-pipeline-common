def call(Map params) {
	def dir = params.dir
	def files = params.webappFiles
	def artifactId = params.artifactId
	def version = params.version
	if(params.artifact) {
			def artifactParts = params.artifact.split(':')
			group = artifactParts[0]
			artifactId = artifactParts[1]
			version = artifactParts[2]
			if(artifactParts.length > 3) {
					type = artifactParts[4]
			}
	}
	
	def zipFile = "${artifactId}-${version}.zip"
	
	new java.io.File(zipFile).delete()
	echo "Zipping webapp into ${zipFile}"
	zip(dir: dir, glob: files, zipFile: zipFile)
	echo "Uploading webapp zip to Nexus (${params.repository})"
	nexusUpload(
		file: zipFile,
		artifact: params.artifact,
		artifactId: params.artifactId,
		type: 'zip',
		group: params.group,
		version: params.version,
		repository: params.repository,
		credentials: params.credentials,
		nexusUrl: params.nexusUrl
	)
	new java.io.File(zipFile).delete()
}